import isMobile from '../mixin/isMobile'

export default ({ app, Vue }) => {
  Vue.mixin(isMobile)
}
