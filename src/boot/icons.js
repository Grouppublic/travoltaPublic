import iconBase from '../components/iconBase'

export default ({ app, Vue }) => {
  Vue.component('iconBase', iconBase)

  Vue.config.productionTip = false
}
