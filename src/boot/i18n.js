import VueI18n from 'vue-i18n'
import en from '../languages/eng.json'
import ru from '../languages/rus.json'

export default ({ app, Vue }) => {
  Vue.use(VueI18n)
  app.i18n = new VueI18n({
    messages: {
      en,
      ru
    }
  })
}
