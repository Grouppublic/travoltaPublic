import state from './mainState'
import getters from './mainGetters'
import mutations from './mainMutations'
import actions from './mainActions'

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}
