export default {
  gallery (state, value) {
    state.gallery = value
  },

  travelOptions (state, value) {
    state.travelOptions = value
  },

  isMobile (state, value) {
    state.isMobile = value
  }
}
