export default {
  galleryAction ({ commit }, value) {
    commit('gallery', value)
  },

  travelOptions ({ commit }, value) {
    commit('travelOptions', value)
  },

  isMobileAction ({ commit }, value) {
    commit('isMobile', value)
  }
}
