import { mapState } from 'vuex'

export default {
  computed: {
    ...mapState({
      isMobileState: state => state.main.isMobile
    }),

    isMobile: function () {
      return this.isMobileState
    }
  }
}
