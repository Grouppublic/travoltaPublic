// Функция добавления (спереди) к цифре нуля. К примеру, для отображения в формате ХХ, минут, дней и пр.
export function addZeroInFront (value) {
  if (value < 10) value = `0${value}`
  return value
}
