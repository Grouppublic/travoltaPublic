export default {
  computed: {
    changeLanguageInQuasar: function () {
      let locale = this.$i18n.locale

      if (locale === 'en') locale = 'en-gb'

      // Меняем язык в компонентах Quasar
      import(`quasar/lang/${locale}`).then(lang => {
        this.$q.lang.set(lang.default)
      })
    }
  }
}
